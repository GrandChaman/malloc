/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_malloc.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/07 14:47:23 by fle-roy           #+#    #+#             */
/*   Updated: 2019/09/19 10:31:35 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#ifndef FT_MALLOC_H
# define FT_MALLOC_H
# include <stdlib.h>

# ifdef DEV
void		*ft_malloc(size_t size);
void		ft_free(void *ptr);
void		*ft_realloc(void *ptr, size_t size);
void		*ft_reallocf(void *ptr, size_t size);
void		*ft_calloc(size_t count, size_t size);
size_t		ft_malloc_good_size(size_t size);
size_t		ft_malloc_size(void	*ptr);
# else
void		*malloc(size_t size);
void		free(void *ptr);
void		*realloc(void *ptr, size_t size);
void		*reallocf(void *ptr, size_t size);
void		*calloc(size_t count, size_t size);
size_t		malloc_good_size(size_t size);
size_t		malloc_size(void *ptr);
# endif
void		show_alloc_mem(void);
#endif