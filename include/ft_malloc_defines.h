/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_malloc_defines.h                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bluff <bluff@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/07 14:47:28 by fle-roy           #+#    #+#             */
/*   Updated: 2019/04/03 14:03:13 by bluff            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_MALLOC_DEFINES_H
# define FT_MALLOC_DEFINES_H
# define FTM_CALLOC							0
# define FTM_VALLOC							1

# define FTM_PREV_OFFSET					0
# define FTM_NEXT_OFFSET					1

# define FTM_TINY_BLOCK_MAX_SIZE			128
# define FTM_SMALL_BLOCK_MAX_SIZE			4096

# define FTM_BLOCK_TINY_STEP				32
# define FTM_BLOCK_SMALL_STEP				128
# define FTM_BLOCK_LARGE_STEP				4096

# define FTM_BLOCK_TINY_SMALL_PAGES			2

# define FTM_BLOCK_TINY						0b00000001
# define FTM_BLOCK_SMALL 					0b00000010
# define FTM_BLOCK_LARGE 					0b00000100
# define HEX_CHARSET 						"0123456789abcdef"

# define FTM_LARGE_HOME_PADDING				(16 + sizeof(size_t) + \
											(sizeof(size_t) % 16))
# define FTM_LARGE_HOME_SIZE_PADDING		(16)

# ifdef DEV
#  define FT_MALLOC(x) ft_malloc(x)
#  define FT_CALLOC(x, y) ft_calloc(x, y)
#  define FT_VALLOC(x) ft_valloc(x)
#  define FT_REALLOC(x, y) ft_realloc(x, y)
#  define FT_REALLOCF(x, y) ft_reallocf(x, y)
#  define FT_FREE(x) ft_free(x)
#  define FT_MALLOC_SIZE(x) ft_malloc_size(x)
#  define FT_MALLOC_GOOD_SIZE(x) ft_malloc_good_size(x)

# else
#  define FT_MALLOC(x) malloc(x)
#  define FT_CALLOC(x, y) calloc(x, y)
#  define FT_VALLOC(x) valloc(x)
#  define FT_REALLOC(x, y) realloc(x, y)
#  define FT_REALLOCF(x, y) reallocf(x, y)
#  define FT_FREE(x) free(x)
#  define FT_MALLOC_SIZE(x) malloc_size(x)
#  define FT_MALLOC_GOOD_SIZE(x) malloc_good_size(x)
# endif

#endif