/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_malloc_internal.h                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/07 14:47:18 by fle-roy           #+#    #+#             */
/*   Updated: 2019/05/02 11:15:35 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_MALLOC_INTERNAL_H
# define FT_MALLOC_INTERNAL_H
# include <sys/mman.h>
# include <unistd.h>
# include <pthread.h>

typedef	struct				s_malloc_home_start
{
	unsigned int			psize;
	void					**home;
}							t_malloc_home_start;

typedef	struct				s_malloc_home
{
	unsigned int			size;
	void					**ptr;
	char					flag;
	unsigned int			step;
}							t_malloc_home;

typedef	struct				s_malloc_record
{
	void					**ptr;
	size_t					size;
	char					used;
}							t_malloc_record;

typedef	struct				s_malloc_record_zone
{
	void					***ptr;
	t_malloc_record			map[256];
	char					flag;
	t_malloc_home			home;
	unsigned int			step;
}							t_malloc_record_zone;

typedef	struct				s_malloc_large_home
{
	void					**ptr;
	char					flag;
	size_t					size;
}							t_malloc_large_home;

char						ft_malloc_zone_type_selector(size_t size);
size_t						ft_malloc_good_size_type(char type, size_t size);
size_t						ft_malloc_get_home_size(char flag, size_t *nb_page,
	size_t *nb_rec_zone);
unsigned int				ft_malloc_step(char type);
typedef char				*t_malloc_zone;
extern t_malloc_home_start	g_ft_malloc_home;
extern pthread_mutex_t		g_ft_malloc_lock;
void						*ft_malloc_routine(size_t size);
void						ft_free_routine(void *ptr);
t_malloc_home_start			ft_malloc_home_init(void);
void						ft_malloc_home_destroy(t_malloc_home home);
t_malloc_home				ft_malloc_home_next(void **start, char search,
	char create);
void						*ft_mmap_zone(size_t size);
char						ft_munmap_zone(void *addr, size_t size);
void						**ft_malloc_home_get_next(void **ptr);
void						**ft_malloc_home_set_next(void **ptr, void **next);
void						**ft_malloc_home_get_prev(void **ptr);
void						**ft_malloc_home_set_prev(void **ptr, void **prev);
t_malloc_home				ft_malloc_home_add_page(char flags);
char						ft_malloc_home_flags(void**ptr, char flag);
t_malloc_record_zone		ft_malloc_record_zone_create(t_malloc_home home,
	size_t offset);
void						*ft_malloc_record_alloc(t_malloc_home home,
	size_t size);
void						*ft_malloc_record_alloc_in_zone(
	t_malloc_record_zone *zone, size_t size);
void						*ft_malloc_dealloc_record(void	*ptr);
void						*ft_malloc_dealloc_record_in_zone(
	t_malloc_record_zone *zone, void *ptr);
void						ft_malloc_record_raw_map_loader(t_malloc_record_zone
	*record_zone, unsigned char read_map[256]);
void						ft_malloc_record_zone_map_save(t_malloc_record_zone
	*record_zone);
void						ft_malloc_record_zone_read_map(
	t_malloc_record_zone *record);
t_malloc_record_zone		ft_malloc_record_zone_read(t_malloc_home home,
	unsigned short offset);
void						ft_malloc_record_zone_foreach(
	void (f)(t_malloc_record_zone));
void						ft_malloc_dealloc_record_zone(
	t_malloc_record_zone record);
t_malloc_record_zone		ft_malloc_record_zone_init(t_malloc_home home,
	size_t offset, void *ptr);
t_malloc_record				ft_malloc_find_record(void *ptr);
t_malloc_record				ft_malloc_find_record_in_zone(
		t_malloc_record_zone zone, void *ptr);
t_malloc_record_zone		ft_malloc_find_record_zone(void	*ptr);
t_malloc_home				ft_malloc_home_first(char search, char create);
void						*ft_realloc_record(void *ptr,
	t_malloc_record_zone zone, size_t nsize);
void						*ft_realloc_routine(void *ptr, size_t size);
void						ft_malloc_alloc_zone_reduce_map(
	t_malloc_record_zone *zone);
void						*ft_realloc_large_home(void **ptr, size_t nsize);
void						*ft_malloc_dealloc_large_home(void **ptr);
void						*ft_malloc_large_home_alloc(size_t size);
void						*ft_malloc_find_large_home(void **ptr);
void						ft_malloc_insert_into_sorted_list(void **ptr);
void						ft_malloc_remove_from_sorted_list(void **ptr);
void						print_ptr(void *ptr);

#endif
