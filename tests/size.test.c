#include <criterion/criterion.h>
#include "ft_malloc_internal.h"
#include "ft_malloc_defines.h"
#include "ft_malloc.h"
#include <unistd.h>

bool	check_power_of16(size_t size)
{
	if (size == 0)
      return (false);
   	while( size != 1)
   	{
      if(size % 2 != 0)
	  	return (false);
		size /= 16;
   	}
   return (true);
}

Test(ZoneSizeSelector, Order)
{
    cr_assert(FTM_TINY_BLOCK_MAX_SIZE > 1);
    cr_assert(FTM_SMALL_BLOCK_MAX_SIZE > FTM_TINY_BLOCK_MAX_SIZE);
    cr_assert(FTM_SMALL_BLOCK_MAX_SIZE > FTM_TINY_BLOCK_MAX_SIZE + 1);
    cr_assert(getpagesize() >= FTM_SMALL_BLOCK_MAX_SIZE);
}

Test(ZoneSizeSelector, PowerOf2)
{
    cr_assert(!(FTM_TINY_BLOCK_MAX_SIZE % 16));
    cr_assert(!(FTM_SMALL_BLOCK_MAX_SIZE % 16));

    cr_assert(!(FTM_BLOCK_TINY_STEP % 16));
    cr_assert(!(FTM_BLOCK_SMALL_STEP % 16));
    cr_assert(!(FTM_BLOCK_LARGE_STEP % 16));
}

Test(ZoneSizeSelector, Zero) {
    cr_assert(ft_malloc_zone_type_selector(0) == FTM_BLOCK_TINY);
}

Test(ZoneSizeSelector, One) {
    cr_assert(ft_malloc_zone_type_selector(1) == FTM_BLOCK_TINY);
}

Test(ZoneSizeSelector, MaxsizeTiny) {
    cr_assert(ft_malloc_zone_type_selector(FTM_TINY_BLOCK_MAX_SIZE) == FTM_BLOCK_TINY);
}

Test(ZoneSizeSelector, MinSmall) {
    cr_assert(ft_malloc_zone_type_selector(FTM_TINY_BLOCK_MAX_SIZE + 1) == FTM_BLOCK_SMALL);
}

Test(ZoneSizeSelector, MinSmallPlusOne) {
    cr_assert(ft_malloc_zone_type_selector(FTM_TINY_BLOCK_MAX_SIZE + 2) == FTM_BLOCK_SMALL);
}

Test(ZoneSizeSelector, MaxSmall) {
    cr_assert(ft_malloc_zone_type_selector(FTM_SMALL_BLOCK_MAX_SIZE) == FTM_BLOCK_SMALL);
}

Test(ZoneSizeSelector, MinLarge) {
    cr_assert(ft_malloc_zone_type_selector(FTM_SMALL_BLOCK_MAX_SIZE + 1) == FTM_BLOCK_LARGE);
}

Test(ZoneSizeSelector, MinLargePlusOne) {
    cr_assert(ft_malloc_zone_type_selector(FTM_SMALL_BLOCK_MAX_SIZE + 2) == FTM_BLOCK_LARGE);
}

Test(ZoneSizeSelector, MaxSize) {
    cr_assert(ft_malloc_zone_type_selector(((size_t)(-1))) == FTM_BLOCK_LARGE);
}



Test(MallocGoodSize, Multiple)
{
	int psize = getpagesize();

	//TINY
	cr_assert(ft_malloc_good_size(0) == FTM_BLOCK_TINY_STEP);
	cr_assert(ft_malloc_good_size(FTM_TINY_BLOCK_MAX_SIZE - 1) == FTM_TINY_BLOCK_MAX_SIZE);
	cr_assert(ft_malloc_good_size(FTM_TINY_BLOCK_MAX_SIZE) == FTM_TINY_BLOCK_MAX_SIZE);
	cr_assert(ft_malloc_good_size(FTM_TINY_BLOCK_MAX_SIZE + 1) == FTM_TINY_BLOCK_MAX_SIZE * 2);
	cr_assert(ft_malloc_good_size(FTM_TINY_BLOCK_MAX_SIZE - 1) == FTM_TINY_BLOCK_MAX_SIZE);
	cr_assert(ft_malloc_good_size(FTM_TINY_BLOCK_MAX_SIZE) == FTM_TINY_BLOCK_MAX_SIZE);

	//SMALL
	cr_assert(ft_malloc_good_size(FTM_TINY_BLOCK_MAX_SIZE + 1) == FTM_BLOCK_SMALL_STEP * 2);
	cr_assert(ft_malloc_good_size(FTM_SMALL_BLOCK_MAX_SIZE - 1) == FTM_SMALL_BLOCK_MAX_SIZE);
	cr_assert(ft_malloc_good_size(FTM_SMALL_BLOCK_MAX_SIZE) == FTM_SMALL_BLOCK_MAX_SIZE);
	cr_assert(ft_malloc_good_size(FTM_SMALL_BLOCK_MAX_SIZE + 1) == FTM_SMALL_BLOCK_MAX_SIZE + psize);

	//LARGE
	cr_assert(ft_malloc_good_size(psize - 1) == psize);
	cr_assert(ft_malloc_good_size(psize) == psize);
	cr_assert(ft_malloc_good_size(psize + 1) == psize * 2);
}