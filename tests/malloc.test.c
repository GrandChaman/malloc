#include <criterion/criterion.h>
#include "ft_malloc_internal.h"
#include "ft_malloc_defines.h"
#include "ft_malloc.h"
#include "libft.h"
#include <stdlib.h>

#define MALLOC_NB 5000

void	write_integrity_string(void **ptr, size_t size)
{
	size_t	i = -1;
	char charset[37] = "0123456789acbdefghijklmnopqrstuvwxyz";

	while (++i < size)
		*(char*)((uintptr_t)ptr + i) = charset[i % sizeof(charset)];
}

void	check_integrity_string(void **ptr, size_t size)
{
	size_t	i = -1;
	char charset[37] = "0123456789acbdefghijklmnopqrstuvwxyz";

	while (++i < size)
		cr_assert(*(char*)((uintptr_t)ptr + i) == charset[i % sizeof(charset)]);
}

Test(Malloc, Malloc)
{
	void *ptr[5];
	int i;

	i = 0;
	while (i < 5)
	{
		ptr[i] = ft_malloc(12);
		write_integrity_string(ptr[i], 12);
		check_integrity_string(ptr[i], 12);
		if (i > 0)
			cr_assert(ptr[i] = ptr[i - 1] + 16);
		else
			cr_assert(((uintptr_t)ptr[i] % 4096) == 0);
		i++;
	}
}

Test(Malloc, MallocAndFree)
{
	void *ptr[5];
	int i;

	i = 0;
	while (i < 5)
	{
		ptr[i] = ft_malloc(12);
		write_integrity_string(ptr[i], 12);
		i++;
	}
	i = 0;
	while (i < 5)
	{
		check_integrity_string(ptr[i], 12);
		ft_free(ptr[i]);
		i++;
	}
}

Test(Malloc, MallocDifferentSize)
{
	void *ptr[3];
	int i;
	int ii;

	ii = 4;
	i = 0;
	while (i < 3)
	{
		ptr[i] = ft_malloc(ft_pow(2, ii));
		write_integrity_string(ptr[i], ft_pow(2, ii));
		i++;
		ii *= 2;
	}
	i = 0;
	while (i < 3)
	{
		check_integrity_string(ptr[i], ft_pow(2, ii));
		ft_free(ptr[i]);
		i++;
	}
}

Test(Malloc, MallocDifferentTinySize)
{
	void *ptr[MALLOC_NB];
	int i;
	int ii;

	ii = 4;
	i = 0;
	while (i < MALLOC_NB)
	{
		ptr[i] = ft_malloc(32 * ((i % 4) + 1));
		write_integrity_string(ptr[i], 32 * ((i % 4) + 1));
		i++;
		ii *= 2;
	}
	i = 0;
	while (i < MALLOC_NB)
	{
		check_integrity_string(ptr[i], 32 * ((i % 4) + 1));
		ft_free(ptr[i]);
		i++;
	}
}

Test(Malloc, GetMallocSize)
{
	void *ptr[3];
	int i;
	int ii;

	ii = 4;
	i = 0;
	while (i < 3)
	{
		ptr[i] = ft_malloc(ft_pow(2, ii));
		write_integrity_string(ptr[i], ft_pow(2, ii));

		i++;
		ii *= 2;
	}
	i = 0;
	ii = 4;
	while (i < 3)
	{
		check_integrity_string(ptr[i], ft_pow(2, ii));
		cr_assert(ft_malloc_size(ptr[i]) >= ft_pow(2, ii));
		ft_free(ptr[i]);
		i++;
		ii *= 2;
	}
}

Test(Malloc, LotsOfMalloc)
{
	void *ptr[100];
	int i;

	i = 0;
	while (i < 100)
	{
		ptr[i] = ft_malloc(i);
		write_integrity_string(ptr[i], i);
		i++;
	}
	i = 0;
	while (i < 100)
	{
		check_integrity_string(ptr[i], i);
		cr_assert(ft_malloc_size(ptr[i]) == ft_malloc_good_size(i));
		ft_free(ptr[i]);
		i++;
	}
}

Test(Realloc, NullPtr)
{
	void *ptr;

	ptr = ft_realloc(NULL, FTM_TINY_BLOCK_MAX_SIZE);
	cr_assert(ft_malloc_size(ptr) == FTM_TINY_BLOCK_MAX_SIZE);
	write_integrity_string(ptr, FTM_TINY_BLOCK_MAX_SIZE);
	check_integrity_string(ptr, FTM_TINY_BLOCK_MAX_SIZE);
	ft_free(ptr);
}

Test(Realloc, NullSize)
{
	void *old_ptr;
	void *ptr;

	old_ptr = ft_malloc(FTM_TINY_BLOCK_MAX_SIZE * 2);
	cr_assert(ft_malloc_size(old_ptr) == FTM_TINY_BLOCK_MAX_SIZE * 2);
	ptr = ft_realloc(old_ptr, 0);
	cr_assert(ft_malloc_size(ptr) == FTM_BLOCK_TINY_STEP);
	write_integrity_string(ptr, FTM_TINY_BLOCK_MAX_SIZE);
	check_integrity_string(ptr, FTM_TINY_BLOCK_MAX_SIZE);
	ft_free(ptr);
}

Test(Realloc, SameSize)
{
	void *ptr[MALLOC_NB];
	void *tmp;
	int i = 0;

	while (i < MALLOC_NB)
	{
		ptr[i] = ft_malloc(i);
		write_integrity_string(ptr[i], i);
		check_integrity_string(ptr[i], i);
		i++;
	}
	i = 0;
	while (i < MALLOC_NB)
	{
		tmp = ft_realloc(ptr[i], i);
		cr_assert(ptr[i] == tmp);
		check_integrity_string(ptr[i], i);
		ft_free(ptr[i]);
		i++;
	}
}

Test(Realloc, TwiceMore)
{
	void *ptr[MALLOC_NB];
	void *tmp;
	int i = 0;

	while (i < MALLOC_NB)
	{
		ptr[i] = ft_malloc(i);
		write_integrity_string(ptr[i], i);
		check_integrity_string(ptr[i], i);
		i++;
	}
	while (i < MALLOC_NB)
	{
		tmp = ft_realloc(ptr[i], i * 2);
		check_integrity_string(ptr[i], i);
		cr_assert(ft_malloc_size(tmp) >= i * 2);
		ft_free(tmp);
		i++;
	}
}

Test(Realloc, TwiceLesser)
{
	void *ptr[MALLOC_NB];
	void *tmp;
	int i = 0;

	while (i < MALLOC_NB)
	{
		ptr[i] = ft_malloc(i);
		write_integrity_string(ptr[i], i);
		check_integrity_string(ptr[i], i);
		i++;
	}
	while (i < MALLOC_NB)
	{
		tmp = ft_realloc(ptr[i], i / 2);
		check_integrity_string(ptr[i], i / 2);
		cr_assert(ft_malloc_size(tmp) >= i / 2);
		ft_free(tmp);
		i++;
	}
}

Test(Realloc, Misc)
{
	void *ptr[MALLOC_NB];
	void *tmp;
	int i = 0;
	char mode = 0;

	while (i < MALLOC_NB)
	{
		ptr[i] = ft_malloc(i);
		write_integrity_string(ptr[i], i);
		check_integrity_string(ptr[i], i);
		i++;
	}
	while (i < MALLOC_NB)
	{
		mode = ((mode + 1) % 2);
		tmp = ft_realloc(ptr[i], mode ? i * 2 : i / 2);
		check_integrity_string(ptr[i], mode ? i : i / 2);
		cr_assert(ft_malloc_size(tmp) >= mode ? i * 2 : i / 2);
		ft_free(tmp);
		i++;
	}
}

Test(Calloc, Zero)
{
	void *ptr;
	size_t size;
	size_t count;

	size = 0;
	count = 0;
	ptr = ft_calloc(size, count);
	write_integrity_string(ptr, FTM_BLOCK_TINY_STEP);
	check_integrity_string(ptr, FTM_BLOCK_TINY_STEP);
	ft_free(ptr);
}

Test(Calloc, One)
{
	void *ptr;
	size_t size;
	size_t count;

	size = 1;
	count = 1;
	ptr = ft_calloc(size, count);
	write_integrity_string(ptr, size * count);
	check_integrity_string(ptr, size * count);
	ft_free(ptr);
}

Test(Calloc, 2Pow4)
{
	void *ptr;
	size_t size;
	size_t count;

	size = 1;
	count = 16;
	ptr = ft_calloc(size, count);
	write_integrity_string(ptr, size * count);
	check_integrity_string(ptr, size * count);
	ft_free(ptr);
}

Test(Calloc, 2Pow6)
{
	void *ptr;
	size_t size;
	size_t count;

	size = 1;
	count = 64;
	ptr = ft_calloc(size, count);
	write_integrity_string(ptr, size * count);
	check_integrity_string(ptr, size * count);
	ft_free(ptr);
}

Test(Calloc, 2Pow8)
{
	void *ptr;
	size_t size;
	size_t count;

	size = 4;
	count = 64;
	ptr = ft_calloc(size, count);
	write_integrity_string(ptr, size * count);
	check_integrity_string(ptr, size * count);
	ft_free(ptr);
}

Test(Calloc, Big)
{
	void *ptr;
	size_t size;
	size_t count;

	size = 500;
	count = 242;
	ptr = ft_calloc(size, count);
	write_integrity_string(ptr, size * count);
	check_integrity_string(ptr, size * count);
	ft_free(ptr);
}