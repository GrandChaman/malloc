#include <criterion/criterion.h>
#include "ft_malloc_internal.h"
#include "ft_malloc_defines.h"
#include "ft_malloc.h"
#include <stdlib.h>
#define PSIZE (getpagesize() / 8) - 128

void	check_zone(void	**ptr)
{
	int	i;

	i = 128;
	while (i < PSIZE)
		*(ptr + i++) = 0;
}

