/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_malloc_size.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bluff <bluff@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/13 11:02:55 by bluff             #+#    #+#             */
/*   Updated: 2019/04/03 14:30:50 by bluff            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_malloc.h"
#include "ft_malloc_internal.h"
#include "ft_malloc_defines.h"
#include "libft.h"


size_t		FT_MALLOC_SIZE(void	*ptr)
{
	t_malloc_record				rec;

	rec = ft_malloc_find_record(ptr);
	if (rec.ptr)
		return (rec.size);
	if ((ptr = ft_malloc_find_large_home(ptr)))
		return (*((size_t*)((uintptr_t)ptr + FTM_LARGE_HOME_SIZE_PADDING)));
	return (0);
}

unsigned int	ft_malloc_step(char type)
{
	if (type & FTM_BLOCK_LARGE)
		return (FTM_BLOCK_LARGE_STEP);
	else if (type & FTM_BLOCK_SMALL)
		return (FTM_BLOCK_SMALL_STEP);
	else
		return (FTM_BLOCK_TINY_STEP);
}

/**
 * @brief Return the type of zone that should be created for an allocation of 
 * size
 * 
 * @param size The size the allocation would be 
 * @return char A bitfield defining which zone is more suitable
 */
char	ft_malloc_zone_type_selector(size_t size)
{
	if (size > FTM_SMALL_BLOCK_MAX_SIZE)
		return (FTM_BLOCK_LARGE);
	else if (size > FTM_TINY_BLOCK_MAX_SIZE)
		return (FTM_BLOCK_SMALL);
	else
		return (FTM_BLOCK_TINY);
}

/**
 * @brief Return a zone size that will be equal or greater than `size` with a
 * specified type
 * 
 * @param type The flag to use
 * @param size The size
 * @return size_t The zone size will be
 */
size_t		ft_malloc_good_size_type(char type, size_t size)
{
	size_t	offset;

	if (!size)
		return (FTM_BLOCK_TINY_STEP);
	offset = 0;
	if (type == FTM_BLOCK_LARGE && (size += FTM_LARGE_HOME_PADDING) 
		% FTM_BLOCK_LARGE_STEP)
		offset = FTM_BLOCK_LARGE_STEP - (size % FTM_BLOCK_LARGE_STEP);
	else if (type == FTM_BLOCK_SMALL && size % FTM_BLOCK_SMALL_STEP)
		offset = FTM_BLOCK_SMALL_STEP - (size % FTM_BLOCK_SMALL_STEP);
	else if (size % FTM_BLOCK_TINY_STEP)
		offset = FTM_BLOCK_TINY_STEP - (size % FTM_BLOCK_TINY_STEP);;
	return (size + offset);
}

/**
 * @brief Return a zone size that will be equal or greater than `size`
 * 
 * @param size The size of the data I wanna store
 * @return size_t The size of the zone that should hold such `size`
 */
size_t		FT_MALLOC_GOOD_SIZE(size_t size)
{
	return (ft_malloc_good_size_type(ft_malloc_zone_type_selector(size), size));
}