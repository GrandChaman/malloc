/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_malloc_utils.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bluff <bluff@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/07 14:02:27 by fle-roy           #+#    #+#             */
/*   Updated: 2019/04/03 18:24:04 by bluff            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_malloc.h"
#include "ft_malloc_internal.h"
#include "ft_malloc_defines.h"
#include "libft.h"

void	*ft_malloc_routine(size_t size)
{
	char	type;
	size_t	good_size;
	void	*res;

	type = ft_malloc_zone_type_selector(size);
	good_size = ft_malloc_good_size_type(type, size);
	ft_malloc_home_init();
	if (type & (FTM_BLOCK_SMALL | FTM_BLOCK_TINY))
		res = ft_malloc_record_alloc(ft_malloc_home_first(type, 1), good_size);
	else
		res = ft_malloc_large_home_alloc(good_size);
	return (res);
}

void	ft_free_routine(void *ptr)
{
	void 	*dealloced_ptr;

	if (!ptr)
		return ;
	(void)dealloced_ptr;
	ft_malloc_home_init();
	if ((dealloced_ptr = ft_malloc_dealloc_record(ptr)) == ptr)
		return ;
	else if (dealloced_ptr == (void*)-1)
		return ;
	ft_malloc_dealloc_large_home(ptr);
}


void	*ft_realloc_routine(void *ptr, size_t size)
{
	t_malloc_record_zone		rec_zone;
	void						*large_home;
	void						*nptr;
	size_t						old_size;


	nptr = NULL;
	old_size = FT_MALLOC_SIZE(ptr);
	rec_zone = ft_malloc_find_record_zone(ptr);

	if (rec_zone.ptr)
		nptr = ft_realloc_record(ptr, rec_zone, size);
	else if ((large_home = ft_realloc_large_home(ptr, size)))
		return (large_home);
	if (ft_malloc_zone_type_selector(old_size) & FTM_BLOCK_LARGE)
		old_size -= FTM_LARGE_HOME_PADDING;
	if (!nptr)
	{
		nptr = ft_malloc_routine(size);
		if (nptr)
			ft_memmove(nptr, ptr, size > old_size ? old_size : size);
		ft_free_routine(ptr);
	}
	return (nptr);
}
