/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_malloc_records_utils.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/18 17:00:03 by bluff             #+#    #+#             */
/*   Updated: 2019/05/02 10:52:40 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_malloc.h"
#include "ft_malloc_internal.h"
#include "ft_malloc_defines.h"
#include "libft.h"

void				ft_malloc_record_zone_map_save_record(unsigned char buf[64],
	unsigned short *offset, unsigned char type, unsigned short size)
{
	unsigned short i;

	i = 0;
	while (i < size)
	{
		buf[*offset / 4] |= (type << (6 - ((*offset % 4) * 2)));
		(*offset)++;
		i++;
	}
}

void				ft_malloc_record_zone_map_save(
	t_malloc_record_zone *record_zone)
{
	unsigned short	i;
	unsigned short	ii;
	unsigned char	buf[64];
	unsigned char	c;
	unsigned short	diff;

	c = 0b00;
	i = 0;
	ft_bzero((void*)buf, 64);
	ii = 0;
	while (ii < 256 && i < 256 && record_zone->map[i].ptr)
	{
		if (i > 0 && ii < 255 && (diff = ((uintptr_t)record_zone->map[i].ptr
				- ((uintptr_t)record_zone->map[i - 1].ptr
				+ record_zone->map[i - 1].size)) / record_zone->step))
			ft_malloc_record_zone_map_save_record(buf, &ii, 0, diff);
		if (record_zone->map[i].used)
			c = (c == 0b01 ? 0b10 : 0b01);
		else
			c = 0b00;
		ft_malloc_record_zone_map_save_record(buf, &ii, c,
			record_zone->map[i].size / record_zone->step);
		i++;
	}
	ft_memcpy((void**)((uintptr_t)record_zone->ptr + 8), (void*)buf, 64);
}

void				ft_malloc_record_raw_map_loader(
	t_malloc_record_zone *record_zone, unsigned char read_map[256])
{
	unsigned char	lborder;
	unsigned char	rborder;
	unsigned short	i;
	unsigned short	ii;
	unsigned char	rchar;

	i = 0;
	ii = 0;
	lborder = 0;
	rborder = 0;
	while (i < 256)
	{
		lborder = i;
		rchar = read_map[i];
		while (i < 256 && read_map[i] == rchar)
			rborder = i++;
		record_zone->map[ii].ptr = (void**)(((uintptr_t)*record_zone->ptr) +
			(lborder * record_zone->step));
		record_zone->map[ii].used = (rchar != 0);
		record_zone->map[ii++].size = ((size_t)rborder - lborder + 1)
			* record_zone->step;
	}
	while (ii < 256)
		record_zone->map[ii++] = (t_malloc_record){.ptr = NULL, .size = 0,
			.used = 0};
}

t_malloc_record		ft_malloc_find_record(void *ptr)
{
	t_malloc_record_zone	zone;

	zone = ft_malloc_find_record_zone(ptr);
	if (!zone.ptr)
		return ((t_malloc_record){.ptr = NULL});
	return (ft_malloc_find_record_in_zone(zone, ptr));
}
