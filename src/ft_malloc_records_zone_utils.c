/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_malloc_records_zone_utils.c                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/25 14:30:03 by bluff             #+#    #+#             */
/*   Updated: 2019/05/02 10:57:11 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_malloc.h"
#include "ft_malloc_internal.h"
#include "ft_malloc_defines.h"
#include "libft.h"

t_malloc_record			ft_malloc_find_record_in_zone(t_malloc_record_zone zone,
	void *ptr)
{
	unsigned short			i;

	i = 0;
	while (i < 256)
		if (zone.map[i].ptr == ptr)
			return (zone.map[i]);
		else
			i++;
	i = 0;
	while (i < 256 && zone.map[i].ptr)
		i++;
	return ((t_malloc_record){.ptr = NULL});
}

t_malloc_record_zone	ft_malloc_find_record_zone(void *ptr)
{
	t_malloc_home			h;
	size_t					offset;
	size_t					nb_page;
	size_t					nb_rec_page;

	h = ft_malloc_home_first(FTM_BLOCK_TINY | FTM_BLOCK_SMALL, 0);
	while (h.ptr)
	{
		ft_malloc_get_home_size(h.flag, &nb_page, &nb_rec_page);
		if ((uintptr_t)ptr >= ((uintptr_t)h.ptr
			+ (nb_page * g_ft_malloc_home.psize))
			&& (uintptr_t)ptr < ((uintptr_t)h.ptr
			+ (nb_page * g_ft_malloc_home.psize))
			+ (nb_rec_page * g_ft_malloc_home.psize))
		{
			offset = (((uintptr_t)ptr - ((uintptr_t)h.ptr + h.size))
				/ (h.step * 256)) * (64 + 8);
			return (ft_malloc_record_zone_read(h, offset + 16));
		}
		h = ft_malloc_home_next(h.ptr, FTM_BLOCK_SMALL | FTM_BLOCK_TINY, 0);
	}
	return ((t_malloc_record_zone){.ptr = NULL});
}

void					ft_malloc_record_zone_read_map(
		t_malloc_record_zone *record)
{
	unsigned char	read_map[256];
	unsigned char	read_char;
	unsigned short	i;
	unsigned short	ii;

	i = 0;
	(void)record;
	ft_bzero((void*)read_map, 256);
	while (i < 64)
	{
		read_char = (char)(*(void***)((uintptr_t)record->ptr + 8 + i));
		ii = -1;
		while (++ii < 4)
		{
			read_map[i * 4 + (3 - ii)] |= ((read_char % 2));
			read_char >>= 1;
			read_map[i * 4 + (3 - ii)] |= ((read_char % 2) << 1);
			read_char >>= 1;
		}
		i++;
	}
	ft_malloc_record_raw_map_loader(record, read_map);
}

t_malloc_record_zone	ft_malloc_record_zone_read(t_malloc_home home,
	unsigned short offset)
{
	t_malloc_record_zone	record;

	record.ptr = ((void***)(((uintptr_t)home.ptr) + offset));
	record.flag = home.flag;
	record.step = home.step;
	record.home = home;
	if (record.ptr)
		ft_malloc_record_zone_read_map(&record);
	return (record);
}

void					ft_malloc_record_zone_foreach(
		void (f)(t_malloc_record_zone))
{
	t_malloc_home	home;
	unsigned short	i;

	home = ft_malloc_home_first(FTM_BLOCK_TINY | FTM_BLOCK_SMALL, 0);
	while (home.ptr)
	{
		i = 16;
		while (i < (home.size - (64 + 8)))
		{
			if ((*(void**)((uintptr_t)home.ptr + i)))
				f(ft_malloc_record_zone_read(home, i));
			i += 8 + 64;
		}
		home = ft_malloc_home_next(home.ptr, FTM_BLOCK_TINY
			| FTM_BLOCK_SMALL, 0);
	}
	return ;
}
