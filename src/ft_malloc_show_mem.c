/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_malloc_show_mem.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/07 14:02:27 by fle-roy           #+#    #+#             */
/*   Updated: 2019/05/02 10:58:22 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_malloc.h"
#include "ft_malloc_internal.h"
#include "ft_malloc_defines.h"
#include "libft.h"

void			print_ptr(void *ptr)
{
	char		i;
	char		buf[65];
	uintptr_t	lptr;

	i = 0;
	lptr = (uintptr_t)ptr;
	ft_bzero(buf, 65);
	while (lptr || i == 0)
	{
		buf[64 - i] = HEX_CHARSET[lptr % 16];
		lptr /= 16;
		i++;
	}
	buf[64 - i++] = 'x';
	buf[64 - i++] = '0';
	write(1, buf + (65 - i), i);
}

void			print_nbr(size_t nbr)
{
	char		buf[21];
	char		i;

	i = 0;
	ft_bzero(buf, 21);
	while (i < 20 && (nbr || !i))
	{
		buf[19 - i] = (nbr % 10) + '0';
		nbr /= 10;
		i++;
	}
	ft_putstr(buf + (20 - (i)));
}

size_t			print_record_zone(t_malloc_record_zone zone)
{
	char							*title[2];
	size_t							i;
	size_t							res;

	title[0] = "TINY :\n";
	title[1] = "SMALL :\n";
	i = -1;
	res = 0;
	while (++i < 256 && zone.map[i].ptr)
		if (zone.map[i].used)
		{
			if (!i)
				ft_putstr(title[zone.flag / 2]);
			print_ptr(zone.map[i].ptr);
			ft_putstr(" - ");
			print_ptr(zone.map[i].ptr + zone.map[i].size / sizeof(void*));
			ft_putstr(": ");
			print_nbr(zone.map[i].size);
			ft_putstr(" octets\n");
			res += zone.map[i].size;
		}
	return (res);
}

size_t			print_large_records(t_malloc_home home)
{
	size_t			res;

	res = 0;
	ft_putstr("LARGE :\n");
	print_ptr(home.ptr);
	ft_putstr(" - ");
	print_ptr(home.ptr + home.size / sizeof(void*));
	ft_putstr(": ");
	print_nbr(home.size);
	ft_putstr(" octets\n");
	res += home.size;
	return (res);
}

void			show_alloc_mem(void)
{
	t_malloc_home					home;
	size_t							size;
	size_t							i;

	size = 0;
	pthread_mutex_lock(&g_ft_malloc_lock);
	home = ft_malloc_home_first((unsigned char)-1, 0);
	while (home.ptr)
	{
		if (home.flag & FTM_BLOCK_LARGE)
			size += print_large_records(home);
		else
		{
			i = 16 - (64 + 8);
			while ((i += (8 + 64)) < ((g_ft_malloc_home.psize
				* FTM_BLOCK_TINY_SMALL_PAGES) - (64 + 8)))
				size += print_record_zone(ft_malloc_record_zone_read(home, i));
		}
		home = ft_malloc_home_next(home.ptr, (unsigned char)-1, 0);
	}
	ft_putstr("Total : ");
	print_nbr(size);
	ft_putstr(" octets\n");
	pthread_mutex_unlock(&g_ft_malloc_lock);
}
