/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_malloc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bluff <bluff@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/07 14:02:27 by fle-roy           #+#    #+#             */
/*   Updated: 2019/04/03 18:23:50 by bluff            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_malloc.h"
#include "ft_malloc_internal.h"
#include "ft_malloc_defines.h"
#include "libft.h"

void		*FT_MALLOC(size_t size)
{
	void	*res;

	pthread_mutex_lock(&g_ft_malloc_lock);
	res = ft_malloc_routine(size);
	pthread_mutex_unlock(&g_ft_malloc_lock);
	return (res);
}

void		FT_FREE(void *ptr)
{
	if (!ptr)
		return ;
	pthread_mutex_lock(&g_ft_malloc_lock);
	ft_free_routine(ptr);
	pthread_mutex_unlock(&g_ft_malloc_lock);
}

void		*FT_REALLOC(void *ptr, size_t size)
{
	void	*nptr;

	nptr = NULL;
	if (!ptr)
		return (FT_MALLOC(size));
	pthread_mutex_lock(&g_ft_malloc_lock);
	nptr = ft_realloc_routine(ptr, size);
	pthread_mutex_unlock(&g_ft_malloc_lock);
	return (nptr);
}
void		*FT_REALLOCF(void *ptr, size_t size)
{
	void	*nptr;

	nptr = NULL;
	pthread_mutex_lock(&g_ft_malloc_lock);
	if (!(nptr = ft_realloc_routine(ptr, size)))
		ft_free_routine(ptr);
	pthread_mutex_unlock(&g_ft_malloc_lock);
	return (nptr);
}

void		*FT_CALLOC(size_t count, size_t size)
{
	size_t	total_size;
	void	*res;

	if (!count || !size)
		total_size = 1;
	else
		total_size = (count * size);
	if ((res = FT_MALLOC(total_size)))
		ft_bzero(res, total_size);
	return (res);
}
