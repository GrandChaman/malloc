/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_malloc_home_utils.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/14 16:09:46 by bluff             #+#    #+#             */
/*   Updated: 2019/05/02 10:42:07 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_malloc.h"
#include "ft_malloc_internal.h"
#include "ft_malloc_defines.h"
#include "libft.h"

size_t					ft_malloc_get_home_size(char flag, size_t *nb_page,
	size_t *nb_rec_zone)
{
	size_t step;

	step = (ft_malloc_step(flag) * 256) / g_ft_malloc_home.psize;
	if (flag & (FTM_BLOCK_TINY | FTM_BLOCK_SMALL))
	{
		*nb_page = FTM_BLOCK_TINY_SMALL_PAGES;
		*nb_rec_zone = (*nb_page * ((g_ft_malloc_home.psize - 16) / (64 + 8)))
			* ((ft_malloc_step(flag) * 256) / g_ft_malloc_home.psize);
	}
	return (step);
}

char					ft_malloc_home_flags(void **ptr, char flag)
{
	uintptr_t offset;
	uintptr_t offset_flag_next;
	uintptr_t prev;
	uintptr_t next;

	prev = (uintptr_t)*(ptr + FTM_PREV_OFFSET);
	next = (uintptr_t)*(ptr + FTM_NEXT_OFFSET);
	offset = (sizeof(void*) - sizeof(char)) * 8;
	offset_flag_next = offset + ((sizeof(char) * 8) / 2);
	if (flag)
	{
		*(ptr + FTM_PREV_OFFSET) = (void*)(((prev >> 4) << 4)
			| ((uintptr_t)(flag >> 4) << 4));
		*(ptr + FTM_NEXT_OFFSET) = (void*)(((next << 4) >> 4)
			| (((uintptr_t)((flag << 4) >> 4) << offset_flag_next)));
		return (flag);
	}
	return ((char)((prev << (offset + ((sizeof(char) * 8) / 2)))
		| (next >> (offset + ((sizeof(char) * 8) / 2)))));
}

t_malloc_home_start		ft_malloc_home_init(void)
{
	if (!g_ft_malloc_home.psize)
		g_ft_malloc_home.psize = getpagesize();
	if (g_ft_malloc_home.home)
		return (g_ft_malloc_home);
	g_ft_malloc_home.home = ft_malloc_home_add_page(FTM_BLOCK_TINY).ptr;
	return (g_ft_malloc_home);
}

void					ft_malloc_home_destroy(t_malloc_home home)
{
	size_t	nb_page;
	size_t	nb_rec_page;

	if (home.ptr)
	{
		ft_malloc_remove_from_sorted_list(home.ptr);
		ft_malloc_get_home_size(home.flag, &nb_page, &nb_rec_page);
		ft_munmap_zone(home.ptr, nb_page + nb_rec_page);
	}
}
