/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_malloc_mem.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/14 17:44:59 by bluff             #+#    #+#             */
/*   Updated: 2019/05/02 10:43:33 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_malloc_internal.h"
#include "ft_malloc_defines.h"
#include "libft.h"

void	*ft_mmap_zone(size_t size)
{
	void	*ptr;
	size_t	psize;

	psize = g_ft_malloc_home.psize;
	size = size * psize;
	ptr = mmap(NULL, size, PROT_READ | PROT_WRITE,
		MAP_PRIVATE | MAP_ANON, -1, 0);
	if (ptr == MAP_FAILED)
		return (NULL);
	return (ptr);
}

char	ft_munmap_zone(void *addr, size_t size)
{
	return (munmap(addr, size * g_ft_malloc_home.psize));
}
