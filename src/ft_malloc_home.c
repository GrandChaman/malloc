/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_malloc_home.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/14 16:09:46 by bluff             #+#    #+#             */
/*   Updated: 2019/05/02 10:40:55 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_malloc.h"
#include "ft_malloc_internal.h"
#include "ft_malloc_defines.h"
#include "libft.h"

t_malloc_home_start	g_ft_malloc_home;
pthread_mutex_t g_ft_malloc_lock = PTHREAD_MUTEX_INITIALIZER;

t_malloc_home	ft_malloc_home_next(void **start, char search, char create)
{
	void			**next_ptr;
	void			**curr_ptr;
	t_malloc_home	home;

	curr_ptr = start;
	next_ptr = ft_malloc_home_get_next(curr_ptr);
	while (next_ptr && (!search
		|| !(ft_malloc_home_flags(next_ptr, 0) & search)))
	{
		next_ptr = ft_malloc_home_get_next(curr_ptr);
		if (!next_ptr)
			break ;
		curr_ptr = next_ptr;
	}
	if (create && !next_ptr)
		return (ft_malloc_home_add_page(search ? search : FTM_BLOCK_LARGE));
	home.ptr = next_ptr;
	home.flag = home.ptr ? ft_malloc_home_flags(home.ptr, 0) : search;
	if (home.ptr && home.flag & FTM_BLOCK_LARGE)
		home.size = *(size_t*)((uintptr_t)home.ptr
			+ FTM_LARGE_HOME_SIZE_PADDING);
	else
		home.size = g_ft_malloc_home.psize * FTM_BLOCK_TINY_SMALL_PAGES;
	home.step = ft_malloc_step(home.flag);
	return (home);
}

t_malloc_home	ft_malloc_home_create(char flags)
{
	size_t			nb_pages;
	size_t			nb_rec_zone;
	size_t			counter;
	size_t			step;
	t_malloc_home	home;

	counter = -1;
	step = ft_malloc_get_home_size(flags, &nb_pages, &nb_rec_zone);
	home.flag = flags;
	home.size = g_ft_malloc_home.psize * nb_pages;
	home.ptr = ft_mmap_zone(nb_pages + nb_rec_zone);
	if (flags & (FTM_BLOCK_TINY | FTM_BLOCK_SMALL))
		while (++counter < (nb_pages
			* ((g_ft_malloc_home.psize - 16) / (64 + 8))))
			*(void**)((uintptr_t)home.ptr
				+ (16 + ((64 + 8) * counter))) = (void*)(uintptr_t)home.ptr
				+ ((nb_pages + (counter * step)) * g_ft_malloc_home.psize);
	if (!home.ptr)
		return (home);
	home.step = ft_malloc_step(flags);
	return (home);
}

t_malloc_home	ft_malloc_home_add_page(char flags)
{
	t_malloc_home	home;

	home = ft_malloc_home_create(flags);
	if (!home.ptr)
		return (home);
	ft_malloc_home_flags(home.ptr, flags ? flags : FTM_BLOCK_LARGE);
	ft_malloc_insert_into_sorted_list(home.ptr);
	return (home);
}

t_malloc_home	ft_malloc_home_first(char search, char create)
{
	t_malloc_home home;

	if (ft_malloc_home_flags(g_ft_malloc_home.home, 0) & search)
	{
		home.ptr = g_ft_malloc_home.home;
		home.flag = ft_malloc_home_flags(g_ft_malloc_home.home, 0);
		if (home.flag & FTM_BLOCK_LARGE)
			home.size = *(size_t*)((uintptr_t)home.ptr
				+ FTM_LARGE_HOME_SIZE_PADDING);
		else
			home.size = g_ft_malloc_home.psize * FTM_BLOCK_TINY_SMALL_PAGES;
		home.step = ft_malloc_step(home.flag);
		return (home);
	}
	return (ft_malloc_home_next(g_ft_malloc_home.home, search, create));
}
