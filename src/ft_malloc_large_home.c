/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_malloc_large_home.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bluff <bluff@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/18 17:00:03 by bluff             #+#    #+#             */
/*   Updated: 2019/04/03 18:28:29 by bluff            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_malloc.h"
#include "ft_malloc_internal.h"
#include "ft_malloc_defines.h"
#include "libft.h"

void	*ft_malloc_find_large_home(void **ptr)
{
	t_malloc_home	home;

	home = ft_malloc_home_first(FTM_BLOCK_LARGE, 0);
	while (home.ptr)
		if (ptr == (void**)((uintptr_t)home.ptr + FTM_LARGE_HOME_PADDING))
			return (home.ptr);
		else
			home = ft_malloc_home_next(home.ptr, FTM_BLOCK_LARGE, 0);	
	return (NULL);
}

void	*ft_realloc_large_home(void **ptr, size_t nsize)
{
	size_t			old_size;

	if ((ptr = ft_malloc_find_large_home(ptr)))
	{
		old_size = *((size_t*)((uintptr_t)ptr + FTM_LARGE_HOME_SIZE_PADDING));
		if (nsize <= old_size)
			return ((void*)(((uintptr_t)ptr + FTM_LARGE_HOME_PADDING)));
	}
	return (NULL);
}

void		*ft_malloc_dealloc_large_home(void **ptr)
{
	if ((ptr = ft_malloc_find_large_home(ptr)))
	{
		ft_malloc_remove_from_sorted_list(ptr);
		ft_munmap_zone(ptr, (*(size_t*)((uintptr_t)ptr + 
			FTM_LARGE_HOME_SIZE_PADDING)) / 4096);
		return (ptr);
	}
	return (NULL);
}

void 		*ft_malloc_large_home_alloc(size_t size)
{
	void			**nhome;

	nhome = ft_mmap_zone((FT_MALLOC_GOOD_SIZE(size) / 4096));
	if (!nhome)
		return (nhome);
	ft_malloc_home_flags(nhome, FTM_BLOCK_LARGE);
	ft_malloc_insert_into_sorted_list(nhome);	
	*((size_t*)((uintptr_t)nhome + FTM_LARGE_HOME_SIZE_PADDING)) = size;
	return (((void*)(uintptr_t)nhome + FTM_LARGE_HOME_PADDING));
}
