/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_malloc_records_free.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/18 17:00:03 by bluff             #+#    #+#             */
/*   Updated: 2019/05/02 10:50:20 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_malloc.h"
#include "ft_malloc_internal.h"
#include "ft_malloc_defines.h"
#include "libft.h"

void			ft_malloc_dealloc_record_zone(t_malloc_record_zone record)
{
	if (!record.ptr)
		return ;
	ft_munmap_zone(*record.ptr, 1);
	ft_bzero(record.ptr, 8 + 64);
}

void			*ft_malloc_dealloc_record_in_zone(t_malloc_record_zone *zone,
	void *ptr)
{
	unsigned short	i;

	i = 0;
	while (i < 256)
		if (zone->map[i].ptr == ptr)
		{
			zone->map[i].used = 0;
			ft_malloc_record_zone_map_save(zone);
			return (ptr);
		}
		else
			i++;
	return (NULL);
}

void			*ft_malloc_try_dealloc_home(t_malloc_home home)
{
	unsigned short			i;
	t_malloc_record_zone	zone;

	if (!home.ptr)
		return (NULL);
	i = 16;
	while (i < (home.size - (64 + 8)))
	{
		if ((*(void**)((uintptr_t)home.ptr + i)))
		{
			zone = ft_malloc_record_zone_read(home, i);
			if (zone.map[0].used || zone.map[1].ptr)
				return (NULL);
		}
		i += 8 + 64;
	}
	ft_malloc_home_destroy(home);
	return (home.ptr);
}

void			*ft_malloc_dealloc_record(void *ptr)
{
	void					*res;
	t_malloc_record_zone	zone;

	zone = ft_malloc_find_record_zone(ptr);
	if (zone.ptr && (res = ft_malloc_dealloc_record_in_zone(&zone, ptr)))
	{
		ft_malloc_try_dealloc_home(zone.home);
		return (res);
	}
	return (NULL);
}
