/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_malloc_home_list_utils.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/02 10:33:20 by fle-roy           #+#    #+#             */
/*   Updated: 2019/05/02 10:34:11 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_malloc.h"
#include "ft_malloc_internal.h"
#include "ft_malloc_defines.h"
#include "libft.h"

void	**ft_malloc_home_get_next(void **ptr)
{
	if (!ptr)
		return (NULL);
	return ((void**)(((uintptr_t)*(ptr + FTM_NEXT_OFFSET)) << 4));
}

void	**ft_malloc_home_set_next(void **ptr, void **next)
{
	uintptr_t mask;
	uintptr_t tmp;

	if (!ptr)
		return (NULL);
	mask = (uintptr_t)(((unsigned char)-1) >> 4)
		<< ((sizeof(void*) * 8) - ((sizeof(char) * 8) / 2));
	tmp = ((uintptr_t)*(ptr + FTM_NEXT_OFFSET));
	tmp &= mask;
	tmp |= ((uintptr_t)next >> 4);
	*(ptr + FTM_NEXT_OFFSET) = (void*)tmp;
	return (next);
}

void	**ft_malloc_home_set_prev(void **ptr, void **prev)
{
	uintptr_t prev_save;
	uintptr_t tmp;

	if (!ptr)
		return (NULL);
	prev_save = (uintptr_t)*(ptr + FTM_PREV_OFFSET);
	tmp = (uintptr_t)prev;
	tmp = (tmp >> 4) << 4;
	prev_save &= ((uintptr_t)((unsigned char)-1) >> 4);
	prev_save |= tmp;
	*(ptr + FTM_PREV_OFFSET) = (void*)prev_save;
	return (prev);
}

void	**ft_malloc_home_get_prev(void **ptr)
{
	if (!ptr)
		return (NULL);
	return ((void **)((((uintptr_t)*ptr + FTM_PREV_OFFSET) >> 4) << 4));
}
