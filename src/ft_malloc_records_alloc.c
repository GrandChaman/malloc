/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_malloc_records_alloc.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/18 17:00:03 by bluff             #+#    #+#             */
/*   Updated: 2019/05/02 10:49:26 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_malloc.h"
#include "ft_malloc_internal.h"
#include "ft_malloc_defines.h"
#include "libft.h"

void					*ft_realloc_record(void *ptr, t_malloc_record_zone z,
	size_t size)
{
	size_t			good_size;
	unsigned short	i;
	char			ntype;

	i = 0;
	ntype = ft_malloc_zone_type_selector(size);
	good_size = ft_malloc_good_size_type(ntype, size);
	while (i < 256 && z.map[i].ptr != ptr)
		i++;
	if (i == 256 || ntype != ft_malloc_zone_type_selector(z.map[i].size))
		return (NULL);
	if (good_size <= z.map[i].size)
	{
		z.map[i].size = good_size;
		ft_malloc_record_zone_map_save(&z);
		return (z.map[i].ptr);
	}
	else if (i < 255 && !z.map[i + 1].used && good_size <= z.map[i + 1].size)
	{
		z.map[i + 1].size = good_size - z.map[i].size;
		z.map[i].size = good_size;
		ft_malloc_record_zone_map_save(&z);
		return (z.map[i].ptr);
	}
	return (NULL);
}

t_malloc_record_zone	ft_malloc_record_zone_init(t_malloc_home home,
	size_t offset, void *ptr)
{
	t_malloc_record_zone	res;

	res.ptr = (void***)((uintptr_t)home.ptr + offset);
	res.flag = home.flag;
	res.step = home.step;
	res.home = home;
	if (!ptr)
		return (res);
	ft_bzero((void*)((uintptr_t)res.ptr + 8), 64);
	ft_malloc_record_zone_read_map(&res);
	return (res);
}

t_malloc_record_zone	ft_malloc_record_zone_create(t_malloc_home home,
	size_t offset)
{
	return (ft_malloc_record_zone_init(home, offset, ft_mmap_zone(1)));
}

void					*ft_malloc_record_alloc_in_zone(t_malloc_record_zone *z,
	size_t size)
{
	unsigned short	offset;
	unsigned short	ii;

	ii = 0;
	offset = 0;
	while (ii < 256 && z->map[ii].ptr)
		if (!z->map[ii].used && z->map[ii].size >= size)
		{
			z->map[ii].used = 1;
			z->map[ii].size = size;
			ft_malloc_record_zone_map_save(z);
			return ((void*)(((uintptr_t)*z->ptr) + offset));
		}
		else
		{
			offset += z->map[ii].size;
			ii++;
		}
	return (NULL);
}

void					*ft_malloc_record_alloc(t_malloc_home home, size_t size)
{
	unsigned short			i;
	void					*res;
	t_malloc_record_zone	zone;

	while (home.ptr)
	{
		i = 16;
		while (i < ((g_ft_malloc_home.psize
			* FTM_BLOCK_TINY_SMALL_PAGES) - (64 + 8)))
		{
			zone = ft_malloc_record_zone_read(home, i);
			if (!zone.ptr)
				return (NULL);
			if ((res = ft_malloc_record_alloc_in_zone(&zone, size)))
				return (res);
			i += (8 + 64);
		}
		home = ft_malloc_home_next(home.ptr, home.flag, 1);
	}
	return (NULL);
}
