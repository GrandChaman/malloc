/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_malloc_home_list.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/02 10:30:10 by fle-roy           #+#    #+#             */
/*   Updated: 2019/05/02 10:38:17 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_malloc.h"
#include "ft_malloc_internal.h"
#include "ft_malloc_defines.h"
#include "libft.h"

void	ft_malloc_remove_from_sorted_list(void **ptr)
{
	void			**prev;
	void			**next;

	prev = ft_malloc_home_get_prev(ptr);
	next = ft_malloc_home_get_next(ptr);
	ft_malloc_home_set_prev(next, prev);
	ft_malloc_home_set_next(prev, next);
	if (ptr == g_ft_malloc_home.home && prev)
		g_ft_malloc_home.home = prev;
	else if (ptr == g_ft_malloc_home.home && next)
		g_ft_malloc_home.home = next;
	else if (ptr == g_ft_malloc_home.home)
		g_ft_malloc_home.home = NULL;
}

void	ft_malloc_insert_into_sorted_list_routine(void **ptr,
	t_malloc_home home, void **next)
{
	while (home.ptr || !g_ft_malloc_home.home)
	{
		if (!next || (ptr > home.ptr && ptr < next))
		{
			ft_malloc_home_set_next(ptr, next);
			ft_malloc_home_set_next(home.ptr, ptr);
			ft_malloc_home_set_prev(ptr, home.ptr);
			ft_malloc_home_set_prev(next, ptr);
			return ;
		}
		else if (ptr < home.ptr)
		{
			ft_malloc_home_set_next(ptr, home.ptr);
			ft_malloc_home_set_next(ft_malloc_home_get_prev(home.ptr), ptr);
			ft_malloc_home_set_prev(ptr, ft_malloc_home_get_prev(home.ptr));
			ft_malloc_home_set_prev(home.ptr, ptr);
			if (home.ptr == g_ft_malloc_home.home)
				g_ft_malloc_home.home = ptr;
			return ;
		}
		home = ft_malloc_home_next(home.ptr, FTM_BLOCK_SMALL | FTM_BLOCK_TINY
			| FTM_BLOCK_LARGE, 0);
		next = ft_malloc_home_get_next(home.ptr);
	}
}

void	ft_malloc_insert_into_sorted_list(void **ptr)
{
	t_malloc_home	home;
	void			**next;

	next = NULL;
	home.ptr = NULL;
	if (g_ft_malloc_home.home)
	{
		home.ptr = g_ft_malloc_home.home;
		next = ft_malloc_home_get_next(home.ptr);
	}
	ft_malloc_insert_into_sorted_list_routine(ptr, home, next);
}
