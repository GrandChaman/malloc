![pipeline](https://gitlab.com/GrandChaman/malloc/badges/master/build.svg "Build status")
![coverage](https://gitlab.com/GrandChaman/malloc/badges/master/coverage.svg "Code coverage")

# malloc

This project aims to recreate some functions of the `malloc` librairy.

## Installation

```sh
make
```

## Testing

```sh
make test

./ft_malloc_test
```
