# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/01/16 13:28:08 by fle-roy           #+#    #+#              #
#    Updated: 2019/05/02 10:33:18 by fle-roy          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

LIBFT_DIR = libft
vpath %.c src
vpath %.c tests
SRC =	ft_malloc.c \
		ft_malloc_records_utils.c \
		ft_malloc_records_free.c \
		ft_malloc_records_alloc.c \
		ft_malloc_large_home.c \
		ft_malloc_size.c \
		ft_malloc_home.c \
		ft_malloc_mem.c \
		ft_malloc_home_utils.c \
		ft_malloc_utils.c \
		ft_malloc_records_zone_utils.c \
		ft_malloc_show_mem.c \
		ft_malloc_home_list.c \
		ft_malloc_home_list_utils.c
INCLUDE= include
OBJ_DIR=obj
DEP_DIR=dep
LIBFT_INCLUDE = $(LIBFT_DIR)/include
CFLAG = -g3 -Wall -Wextra -Werror -I $(LIBFT_INCLUDE) -I $(INCLUDE)
CC = cc
LFLAG = -lpthread
LFLAG_LIB = -dynamiclib
TEST_CFLAG = -g3 -I $(LIBFT_INCLUDE) -I $(INCLUDE)
TEST_LFLAG = -lcriterion -fprofile-arcs -ftest-coverage
TEST_BIN = ft_malloc_test
TEST_SRC =	size.test.c \
			malloc.test.c \
			home.test.c
OBJ = $(SRC:%.c=$(OBJ_DIR)/%.o)
DEP = $(SRC:%.c=$(DEP_DIR)/%.d)
OBJ_TEST = $(TEST_SRC:%.test.c=$(OBJ_DIR)/%.test.o)
DEP_TEST = $(TEST_SRC:%.test.c=$(DEP_DIR)/%.test.d)
TEST_BIN = ft_malloc_test
ifeq ($(DEV),true)
	CFLAG += -DDEV
	TEST_CFLAG += -DDEV
endif

ifeq ($(OPTI),true)
	CFLAG += -O3
	TEST_CFLAG += -O3
endif

ifeq ($(HOSTTYPE),)
	HOSTTYPE := $(shell uname -m)_$(shell uname -s)
endif
ifeq ($(CODE_COVERAGE),true)
	CFLAG += -fprofile-arcs -ftest-coverage
	TEST_CFLAG += -fprofile-arcs -ftest-coverage
endif
NAME = libft_malloc.so
LINK = libft_malloc_$(HOSTTYPE).so
NAME_UP = MALLOC
LIBFT = $(addprefix $(LIBFT_DIR)/, bin/libft.a)
all: lib $(NAME)
lib: 
	@$(MAKE) -C $(LIBFT_DIR)
test: $(TEST_BIN)
$(LIBFT):
	@$(MAKE) -C $(LIBFT_DIR)
$(OBJ_DIR)/%.test.o: %.test.c
	@printf "\r\033[K[$(NAME_UP)] \033[1;32mBuilding $<\033[0m"
	@$(CC) $(TEST_CFLAG) -c $< -o $@
$(DEP_DIR)/%.test.d: %.test.c
	@printf "\r\033[K[$(NAME_UP)] \033[1;32mGenerating dependencies - $<\033[0m"
	@$(CC) $(TEST_CFLAG) -MM $^ | sed -e '1s/^/$(OBJ_DIR)\//' > $@
$(OBJ_DIR)/%.o: %.c
	@printf "\r\033[K[$(NAME_UP)] \033[1;32mBuilding $<\033[0m"
	@$(CC) $(CFLAG) -c $(CFLAG) $< -o $@
$(DEP_DIR)/%.d: %.c
	@printf "\r\033[K[$(NAME_UP)] \033[1;32mGenerating dependencies - $<\033[0m"
	@$(CC) $(CFLAG) -MM $^ | sed -e '1s/^/$(OBJ_DIR)\//' > $@
$(TEST_BIN): $(LIBFT) $(NAME) $(OBJ_TEST)
	@$(CC) $(TEST_CFLAG) $(NAME) $(TEST_LFLAG) $(LFLAG) $(OBJ_TEST) $(LIBFT) -o $(TEST_BIN)
	@printf "\r\033[K[$(NAME_UP)] \033[1;32mTests done!\033[0m\n"
$(NAME): $(LIBFT) $(SRC_LIBFT) $(SRC_PRINTF) $(SRC_GNL) $(OBJ)
	@printf "\r\033[K[$(NAME_UP)] \033[1;32mLinking...\033[0m"
	@$(CC) $(CFLAG) $(LFLAG) $(LFLAG_LIB) $(OBJ) $(LIBFT) -o $(NAME)
	@printf "\r\033[K[$(NAME_UP)] \033[1;32mCompiled!\033[0m\n"
	@rm -rf $(LINK)
	@ln -s $(NAME) $(LINK)
	@printf "\r\033[K[$(NAME_UP)] \033[1;32mDone!\033[0m\n"
clean:
	@$(MAKE) -C $(LIBFT_DIR) clean
	@rm -f $(OBJ) $(OBJ_TEST)
	@printf "[$(NAME_UP)] \033[1;31mCleaned .o!\033[0m\n"
dclean:
	@rm -f $(DEP)
	@printf "[$(NAME_UP)] \033[1;31mCleaned .d!\033[0m\n"
fclean: clean
	@$(MAKE) -C $(LIBFT_DIR) fclean
	@rm -f $(NAME) $(LINK) $(TEST_BIN)
	@printf "[$(NAME_UP)] \033[1;31mCleaned .a!\033[0m\n"
re:
	@$(MAKE) fclean
	@$(MAKE) all
-include $(DEP)
-include $(DEP_TEST)
.PHONY: all clean fclean re dclean test test_setup
